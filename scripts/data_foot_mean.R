
# data foot ----
# pour toutes les données

pacman::p_load(sf, dplyr, mapview)

raw_data_foot_bayonne <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_bayonne_foot_10min_20240530.gpkg")
raw_data_foot_besancon <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_besancon_foot_10min_20240530.gpkg")
raw_data_foot_beziers <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_beziers_foot_10min_20240530.gpkg")
raw_data_foot_bordeaux <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_bordeaux_foot_10min_20240530.gpkg")
raw_data_foot_chamonix <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_chamonix_foot_10min_20240530.gpkg")
raw_data_foot_clermont <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_clermont_foot_10min_20240530.gpkg")
raw_data_foot_colmar <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_colmar_foot_10min_20240530.gpkg")
raw_data_foot_deauville <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_deauville_foot_10min_20240530.gpkg")
raw_data_foot_limoges <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_limoges_foot_10min_20240530.gpkg")
raw_data_foot_marseille <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_marseille_foot_10min_20240530.gpkg")
raw_data_foot_nantes <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_nantes_foot_10min_20240530.gpkg")
raw_data_foot_nimes <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_nimes_foot_10min_20240530.gpkg")
raw_data_foot_paimpol <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_paimpol_foot_10min_20240530.gpkg")
raw_data_foot_reims <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_reims_foot_10min_20240530.gpkg")
raw_data_foot_rennes <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_rennes_foot_10min_20240530.gpkg")
raw_data_foot_stpol <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_stpol_foot_10min_20240530.gpkg")
raw_data_foot_tours <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_tours_foot_10min_20240530.gpkg")


raw_data <- raw_data_foot_bayonne
## moyenne ----
names(raw_data)
mapview::mapview(raw_data)
# Calcul des moyennes pour chaque colonne numérique

average_data <- raw_data %>%
  summarise(
    moyenne_population = mean(sum_population_carreau_decoup, na.rm = TRUE),
    moyenne_longueur_route = mean(longueur_route, na.rm = TRUE),
    # moyenne_nombre_ronds_points = mean(nombre_ronds_points, na.rm = TRUE),
    # moyenne_PN = mean(passage_niveau, na.rm = TRUE),
    # moyenne_longueur_pistes_cyclables = mean(longueur_pistes_cyclables, na.rm = TRUE),
    # moyenne_ZA = mean(ZA, na.rm = TRUE),
    # moyenne_superficie_vegetation = mean(superficie_vegetation, na.rm = TRUE),
    # # moyenne_nombre_arrets = mean(nombre_arrets, na.rm = TRUE),
    # moyenne_nombre_arrets_v2 = mean(nombre_arrets_v2, na.rm = TRUE),
    # moyenne_nombre_parkings = mean(nombre_parkings, na.rm = TRUE),
    # moyenne_sum_lits = mean(sum_lits, na.rm = TRUE),
    # moyenne_nombre_commerces = mean(nombre_commerces, na.rm = TRUE),
    # moyenne_nombre_loisirs = mean(nombre_loisirs, na.rm = TRUE),
    # moyenne_nombre_restauration = mean(nombre_restauration, na.rm = TRUE),
    # moyenne_nombre_sports = mean(nombre_sports, na.rm = TRUE),
    # moyenne_nombre_sante = mean(nombre_sante, na.rm = TRUE),
    moyenne_nombre_eleves = mean(nombre_eleves, na.rm = TRUE)
    # moyenne_pente_moyenne = mean(pente_moyenne, na.rm = TRUE)
    
  ) %>% 
  st_drop_geometry()

glimpse(average_data)

## médiane ----

average_data <- raw_data %>%
  summarise(
    mediane_population = median(sum_population_carreau_decoup, na.rm = TRUE),
    mediane_longueur_route = median(longueur_route, na.rm = TRUE),
    # mediane_nombre_ronds_points = median(nombre_ronds_points, na.rm = TRUE),
    # mediane_PN = median(passage_niveau, na.rm = TRUE),
    # mediane_longueur_pistes_cyclables = median(longueur_pistes_cyclables, na.rm = TRUE),
    # mediane_ZA = median(ZA, na.rm = TRUE),
    # mediane_superficie_vegetation = median(superficie_vegetation, na.rm = TRUE),
    # # mediane_nombre_arrets = median(nombre_arrets, na.rm = TRUE),
    # mediane_nombre_arrets_v2 = median(nombre_arrets_v2, na.rm = TRUE),
    # mediane_nombre_parkings = median(nombre_parkings, na.rm = TRUE),
    # mediane_sum_lits = median(sum_lits, na.rm = TRUE),
    # mediane_nombre_commerces = median(nombre_commerces, na.rm = TRUE),
    # mediane_nombre_loisirs = median(nombre_loisirs, na.rm = TRUE),
    # mediane_nombre_restauration = median(nombre_restauration, na.rm = TRUE),
    # mediane_nombre_sports = median(nombre_sports, na.rm = TRUE),
    # mediane_nombre_sante = median(nombre_sante, na.rm = TRUE),
    mediane_nombre_eleves = median(nombre_eleves, na.rm = TRUE)
    # mediane_pente_moyenne = median(pente_moyenne, na.rm = TRUE)
  ) %>% 
  st_drop_geometry()

glimpse(average_data)




# FOOT
data_foot <- sf::st_read('data/all_data_PKetGARES_tours_car_10min_20240607.gpkg')
data_rennes <- data_foot$total_sum_allegee
nb_classes <- 5 # Par exemple, pour définir 5 classes
jenks_classes <- classInt::classIntervals(data_rennes, 
                                          n = nb_classes, 
                                          style ="jenks")
data_foot$data_rennes_classes <- cut(data_rennes, breaks = jenks_classes$brks, include.lowest = TRUE, labels = FALSE)
table(data_foot$data_rennes_classes)
