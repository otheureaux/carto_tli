---
title: "Metzeral/Colmar/Fribourg"
output:
  html_document:
    css: style.css
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
<!-- Barre de navigation -->
<div class="container">
<div class="nav-vertical">
<a href="index.html">Présentation</a><br>
<a href="bayonne.html">Bayonne/St-Jean</a><br>
<a href="besancon.html">Besan/Chaux-de-Fonds</a><br>
<a href="beziers.html">Béziers/Neussargues</a><br>
<a href="bordeaux.html">Bordeaux/Le Verdon</a><br>
<a href="clermont.html">Clermont/Volvic</a><br>
<a href="saint-pol.html">Etoile-Saint-Pol</a><br>
<a href="deauville.html">Deauville/Cabourg</a><br>
<a href="limoges.html">Limoges/Angoulême</a><br>
<a href="marseille.html">Marseille/Miramas</a><br>
<a href="colmar.html">Metzeral/Fribourg</a><br>
<a href="nantes.html">Nantes/Pornic</a><br>
<a href="nimes.html">Nimes/Grau-du-Roi</a><br>
<a href="paimpol.html">Paimpol/Carhaix</a><br>
<a href="reims.html">Reims/Epernay</a><br>
<a href="rennes.html">Rennes/Châteaubriant</a><br>
<a href="chamonix.html">St-Gervais/Vallorcine</a><br>
<a href="tours.html">Tours/Loches</a>
<!-- Plus de liens selon les sections -->
</div>

<!-- Contenu principal -->
<div class="content">
## Cartographie

<!-- <h1 id="Saint Pol">Autre</h1> -->
<!-- <p>Plus de bla bla bla</p> -->


Chargement des données :  

```{r, out.width='100%', echo = F, warning=F, message=F, include=FALSE}
pacman::p_load(sf, dplyr, tmap, classInt)

# isochrone
isochrone_10min <- sf::st_read('data/isochrones_colmar_foot_10min_20240325.gpkg')

data_foot <- sf::st_read('data/all_data_PKetGARES_colmar_foot_10min_20240607.gpkg')
# vecteur de données
data_rennes <- data_foot$total_sum_allegee
# Appliquer la méthode de Jenks
nb_classes <- 5 # Par exemple, pour définir 5 classes
jenks_classes <- classIntervals(data_rennes, n = nb_classes, style = "jenks")

# Afficher les intervalles de classe
print(jenks_classes$brks)

# Utilisation des intervalles pour classer les données
data_foot$data_rennes_classes <- cut(data_rennes, breaks = jenks_classes$brks, include.lowest = TRUE, labels = FALSE)
data_foot_c4 <- data_foot %>% 
  filter(data_rennes_classes == 4)
data_foot_c5 <- data_foot %>% 
  filter(data_rennes_classes == 5)

# BIKE

data_bike <- sf::st_read('data/all_data_PKetGARES_colmar_bike_10min_20240607.gpkg')
# Votre vecteur de données
data_rennes <- data_bike$total_sum_allegee
# Appliquer la méthode de Jenks
nb_classes <- 5 # Par exemple, pour définir 5 classes
jenks_classes <- classIntervals(data_rennes, n = nb_classes, style = "jenks")

# Afficher les intervalles de classe
print(jenks_classes$brks)

# Utilisation des intervalles pour classer les données
data_bike$data_rennes_classes <- cut(data_rennes, breaks = jenks_classes$brks, include.lowest = TRUE, labels = FALSE)

# CAR

data_car <- sf::st_read('data/all_data_PKetGARES_colmar_car_10min_20240607.gpkg')
# Votre vecteur de données
data_rennes <- data_car$total_sum_allegee
# Appliquer la méthode de Jenks
nb_classes <- 5 # Par exemple, pour définir 5 classes
jenks_classes <- classIntervals(data_rennes, n = nb_classes, style = "jenks")

# Afficher les intervalles de classe
print(jenks_classes$brks)

# Utilisation des intervalles pour classer les données
data_car$data_rennes_classes <- cut(data_rennes, breaks = jenks_classes$brks, include.lowest = TRUE, labels = FALSE)
```


```{r, out.width='100%', echo = F, warning=F, message=F, include=FALSE, cache = T}
gares <- sf::st_read('data/gares_2154_20240529.gpkg') %>% 
  st_transform(4326) %>% 
  st_as_sf(coords = c("lon", "lat"), crs = 4326)
buffer <- st_buffer(data_car, 1000) %>% 
  st_union() %>% 
  st_transform(4326)
mapview::mapview(buffer)
gares<- st_intersection(gares, buffer)
# Initialiser une liste pour stocker les résultats des isochrones
list_isochrones <- list()
# Boucle pour calculer les isochrones pour chaque gare
for (i in 1:nrow(gares)) {
  point <- gares[i, ]
  # Calculer l'isochrone de 10 minutes à pied
  isochrone <- osrm::osrmIsochrone(loc = point, returnclass = "sf", 
                                   breaks = 10,osrm.profile = 'foot')
  # Stocker l'isochrone dans la liste
  list_isochrones[[i]] <- isochrone
}
# Fusionner tous les isochrones dans un seul objet sf si nécessaire
all_isochrones <- do.call(rbind, list_isochrones)
```


```{r, echo=FALSE}
# Définir les couleurs pour les classes - ajustez selon vos préférences
couleurs <- c("#ffffd4ff", "#fed98eff", "#fe9929ff", "#d95f0eff", "#993404ff")
```


```{r, include=FALSE}
tmap_mode('view')
```


```{r, out.width='100%', out.height='800px'}
tm_basemap(c(leaflet::providers$Esri.WorldTopoMap,
             leaflet::providers$OpenStreetMap, 
             leaflet::providers$GeoportailFrance.orthos)) + 
  
  tm_shape(st_make_valid(all_isochrones),
   name = "Isochrones 10min à pied") + tm_borders() + 
 tm_shape(data_car) + tm_dots(col = "data_rennes_classes", 
                               palette = couleurs, 
                               title = "Types d'emplacement",
                               legend.show = TRUE,
                               size = 0.9,
                               labels = c("Très peu favorable", "Peu favorable", "Moyennement favorable", "Favorable", "Très favorable")
                               ) +
  tm_shape(data_bike) + tm_dots(col = "data_rennes_classes", 
                                palette = couleurs, title = "Types d'emplacement",
                                legend.show = TRUE,
                                size = 0.5,
                                labels = c("Très peu favorable", "Peu favorable", "Moyennement favorable", "Favorable", "Très favorable")) + 
  tm_shape(data_foot) + tm_dots(col = "data_rennes_classes", 
                                palette = couleurs, title = "Types d'emplacement",
                                legend.show = TRUE, 
                                size = 0.1,
                                labels = c("Très peu favorable", "Peu favorable", "Moyennement favorable", "Favorable", "Très favorable")) +
        tm_shape(data_foot_c4) + tm_dots(size = 0.3, "#d95f0eff") +  
  tm_shape(data_foot_c5) + tm_dots(size = 0.3, "#993404ff") +
  tm_shape(gares) + tm_dots() +
  tm_text("Intitulé.gare", size = 1, fontface = 2,
          just = "top", ymod = 0.5) +
  tm_scale_bar()
          
```

```{r, echo =F, message=FALSE, , include=FALSE}
list.files('C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/', 
           pattern = 'points_kms_decoup_join_colmar')

raw_data_foot <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_colmar_foot_10min_20240530.gpkg")

raw_data_bike <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_colmar_bike_10min_20240516.gpkg")

raw_data_car <- sf::st_read("C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/POINTS_KMS_DECOUP_JOIN/points_kms_decoup_join_colmar_car_10min_20240516.gpkg")
```


```{r}
glimpse(raw_data_foot)
glimpse(data_foot)
glimpse(raw_data_bike)
glimpse(data_bike)
glimpse(raw_data_car)
glimpse(data_car)
```

</div>