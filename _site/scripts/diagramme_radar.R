
pacman::p_load(sf, dplyr, tmap, classInt,
               fmsb, # diagramme en radar
               scales
)
# data_st_foot_10_st_pol <- sf::st_read('data/all_data_standardized_stpol_foot_10min_20240322.gpkg')
# data_st_foot_10_rennes <- sf::st_read('data/all_data_standardized_rennes_foot_10min_20240322.gpkg')
# data_st_foot_chamonix <- sf::st_read('data/all_data_standardized_chamonix_foot_10min_20240322.gpkg')
# data_st_foot_bayonne <- sf::st_read('data/all_data_standardized_bayonne_foot_10min_20240322.gpkg')

data_st_foot_10_st_pol <- sf::st_read('C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/points_kms_decoup_join_stpol_foot_10min_20240307.gpkg')
data_st_foot_10_rennes <- sf::st_read('C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/points_kms_decoup_join_rennes_foot_10min_20240307.gpkg')
data_st_foot_10_rennes <- data_st_foot_10_rennes[1:60,]
data_st_foot_chamonix <- sf::st_read('C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/points_kms_decoup_join_chamonix_foot_10min_20240318.gpkg')
data_st_foot_bayonne <- sf::st_read('C:/Users/otheureaux/Documents/OT/6T/R/database_sf/processed_data/points_kms_decoup_join_bayonne_foot_10min_20240318.gpkg')

# data for radarchart function version 1 series, minimum value must be omitted from above.
RNGkind("Mersenne-Twister")
set.seed(123)

dat_stpol <- data.frame(
  longueur_route=mean(data_st_foot_10_st_pol$longueur_route),
  sum_lits=mean(data_st_foot_10_st_pol$sum_lits),
  nombre_commerces=mean(data_st_foot_10_st_pol$nombre_commerces),
  nombre_arrets=mean(data_st_foot_10_st_pol$nombre_arrets),
  nombre_parkings=mean(data_st_foot_10_st_pol$nombre_parkings))

dat_rennes <- data.frame(
  longueur_route=mean(data_st_foot_10_rennes$longueur_route),
  sum_lits=mean(data_st_foot_10_rennes$sum_lits),
  nombre_commerces=mean(data_st_foot_10_rennes$nombre_commerces),
  nombre_arrets=mean(data_st_foot_10_rennes$nombre_arrets),
  nombre_parkings=mean(data_st_foot_10_rennes$nombre_parkings))

dat_chamonix <- data.frame(
  longueur_route=mean(data_st_foot_chamonix$longueur_route),
  sum_lits=mean(data_st_foot_chamonix$sum_lits),
  nombre_commerces=mean(data_st_foot_chamonix$nombre_commerces),
  nombre_arrets=mean(data_st_foot_chamonix$nombre_arrets),
  nombre_parkings=mean(data_st_foot_chamonix$nombre_parkings))

dat_bayonne <- data.frame(
    longueur_route=mean(data_st_foot_bayonne$longueur_route),
    sum_lits=mean(data_st_foot_bayonne$sum_lits),
    nombre_commerces=mean(data_st_foot_bayonne$nombre_commerces),
    nombre_arrets=mean(data_st_foot_bayonne$nombre_arrets),
    nombre_parkings=mean(data_st_foot_bayonne$nombre_parkings))

dat_bind <- rbind(dat_stpol, dat_rennes, dat_chamonix, dat_bayonne)

# 
# maxmin2 <- data.frame(
#   longueur_route=c(max(data_st_foot_10_rennes$longueur_route), 
#                    min(data_st_foot_10_rennes$longueur_route)),
#   # sum_lits=c(max(data_st_foot_10_rennes$sum_lits), 
#   #            min(data_st_foot_10_rennes$sum_lits)),
#   # nombre_commerces=c(max(data_st_foot_10_rennes$nombre_commerces), 
#   #                    min(data_st_foot_10_rennes$nombre_commerces)),
#   nombre_arrets=c(max(data_st_foot_10_rennes$nombre_arrets), 
#                   min(data_st_foot_10_rennes$nombre_arrets)),
#   nombre_parkings=c(max(data_st_foot_10_rennes$nombre_parkings), 
#                     min(data_st_foot_10_rennes$nombre_parkings)))
# 
# 
# # data for radarchart function version 1 series, minimum value must be omitted from above.
# RNGkind("Mersenne-Twister")
# set.seed(123)
# dat2 <- data.frame(
#   longueur_route=(data_st_foot_10_rennes$longueur_route),
#   # sum_lits=data_st_foot_10_rennes$sum_lits,
#   # nombre_commerces=data_st_foot_10_rennes$nombre_commerces,
#   nombre_arrets=data_st_foot_10_rennes$nombre_arrets,
#   nombre_parkings=data_st_foot_10_rennes$nombre_parkings)
# 
# dat_bind2 <- rbind(maxmin2, dat2)
# 
# 
# VARNAMES <- c("route", 
#               # "lits", "commerces", 
#               "arrêts", "parking")
op <- par(mar=c(1, 2, 2, 1), mfrow=c(2, 1))

# radarchart(dat, axistype=0, seg=5, plty=1, vlabels=VARNAMES, 
           # title="(axis=1, 5 segments, with specified vlabels)", vlcex=0.5)

# saint pol
# radarchart(dat_bind, axistype=0, pcol=topo.colors(3), 
#            plty=1, # type de ligne
#            pfcol=adjustcolor(topo.colors(3), 0.5), 
#            title="(topo.colors, fill with transparency, axis=0)")

# rennes
radarchart(dat_bind, axistype=0, pcol=topo.colors(3), 
           plty=1, # type de ligne
           pfcol=adjustcolor(topo.colors(3), 0.3), 
           title="(topo.colors, fill with transparency, axis=0)")# rennes
radarchart(dat_bind, axistype=0, pcol=topo.colors(5), 
           plty=1, # type de ligne
           pfcol=adjustcolor(topo.colors(5), 0.3), 
           title="(topo.colors, fill with transparency, axis=0)")

# Supposons que dat_bind est votre dataframe original
# Calculer les minimums et les maximums pour chaque colonne
dat_min <- sapply(dat_bind, min)
dat_max <- sapply(dat_bind, max)

# Ajouter ces lignes au début et à la fin de votre dataframe
dat_bind2 <- rbind(dat_max, dat_bind, dat_min)

# Ensuite, utilisez dat_bind2 avec radarchart
radarchart(dat_bind2, axistype=0, pcol=topo.colors(3), 
           plty=1, pfcol=adjustcolor(topo.colors(3), 0.3), 
           title="(topo.colors, fill with transparency, axis=0)")


# Votre dataframe original
dat_bind <- data.frame(
  longueur_route = c(3919.921, 4187.194, 5625.282,3256.827),
  sum_lits = c(78.74286, 237, 3492.05263, 294.94340),
  nombre_commerces = c(1.328571, 1.550000, 3.526316, 0.6603774),
  nombre_arrets = c(6.685714, 10.616667, 7.815789, 6.924528),
  nombre_parkings = c(147.79286, 45.41667, 233.86842, 27.56604)
)

# Calculer les valeurs minimales et maximales pour chaque variable
min_values <- apply(dat_bind, 2, min)
max_values <- apply(dat_bind, 2, max)

# Ajouter ces valeurs minimales et maximales au dataframe
dat_bind_adjusted <- rbind(max_values, dat_bind, min_values)

# Utiliser dat_bind_adjusted avec radarchart
radarchart(dat_bind_adjusted, axistype=0, pcol=topo.colors(3), 
           plty=1, pfcol=adjustcolor(topo.colors(3), 0.3), 
           title="(topo.colors, fill with transparency, axis=0)")

# Présumons que dat_bind_adjusted est votre dataframe préparé
# Et que vous avez déjà exécuté radarchart comme montré précédemment

# Génération du graphique en radar
radarchart(dat_bind_adjusted, axistype=0, pcol=topo.colors(4),
           plty=1, pfcol=adjustcolor(topo.colors(4), 0.3),
           title="(topo.colors, fill with transparency, axis=0)")

# Ajout de la légende
legend("topright",           # Position de la légende (topright, topleft, bottomright, etc.)
       legend = c("Saint Pol", "Rennes", "Chamonix", "Bayonne"), # Labels pour chaque ligne/commune
       col = topo.colors(4),  # Couleurs correspondant à celles utilisées dans le graphique
       pch = 20,              # Type de point, 20 correspond à un point plein
       pt.cex = 2,            # Taille des points dans la légende
       cex = 0.8,             # Taille du texte de la légende
       bty = "n",             # Type de bordure pour la légende; "n" signifie pas de bordure
       fill = adjustcolor(topo.colors(3), 0.3)) # Couleurs de remplissage avec transparence


par(mfrow=c(1,1))
